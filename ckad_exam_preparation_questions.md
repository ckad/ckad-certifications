# Certified Kubernetes Application Developer (CKAD) Practice Questions.
 
1. Create a pod with the following characteristics, and leave it
    running when complete:
	  - The pod must run in the web namespace. The namespace has already been created
	  - The name .....
	  - Use the  ifccncf/redis image with the 3.2 tag
	  - Expose port 80
2.  Create a secret named another-secret with a key/value pair; key1/value4
	 -  Start an nginx pod named nginx-secret using container image nginx, and add an environment variable exposing the value of the secret key key 1, 
	 - using COOL_VARIABLE as the name for the 	environment variable inside the pod
3.  You are required to create a pod that requests a certain amount of CPU and memory
	 - Create a pod named nginx-resources in the pod-resources namespace that requests a minimum of
	200m CPU and 1Gi memory for its container
4. Create a new deployment for running.nginx with the following parameters;
	 - Run the deployment in the ckad18018 namespace. The namespace has already been created
	 - Name the deployment 4 replicas
	 - Configure the pod with a container image of lfccncf/nginx:1.13.7
	 - Set an environment variable of NGINX__PORT=8080 and also expose that port for the container
above
5. As a Kubernetes application developer complete the following:
	 - Update the app deployment in the kdpd00202 namespace with a maxSurge of 8% and a maxUnavailable of 15%
	 - Perform a rolling update of the web1 deployment, changing the Ifccncf/ngmx image version to 1.13
	 - Roll back the app deployment to the previous version
6. Create a YAML formatted Kubernetes manifest /opt/KDPD00301/periodic.yaml that runs the
following shell command: date in a single busybox container. The command should run five minute
and must complete within 22 seconds
	 - activeDeadlineSeconds: 
	 - successfulJobsHistoryLimit
	 - failedJobsHistoryLimit
7. Ingress 
	 - Go to Deployment, service and ingress file location
	 - Check the Port number of pod, servoice and Ingres
	 - Add host url
	 - Correct service name and apply service & ingress yaml again
8. A Deployment named backend-deployment in namespace staging runs a web application on port
	 - The Deployment's manifest files can be found at -/spicy-pikachu/backend-deployment.yaml.
	 - Modify the Deployment specifying a readiness probe using path /healthz.
	 - Set initialDelaySeconds to 8 and periodSeconds to 5.
